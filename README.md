# NeuralArt-in-pytorch

This reprository is the code implementation in pytorch of paper on neural style transfer which popularly known as neural art
using pytorch VGG16 pretrained model and link for the paper [here](https://www.cvfoundation.org/openaccess/content_cvpr_2016/papers/Gatys_Image_Style_Transfer_CVPR_2016_paper.pdf)

Some results after running that algorithm on Tesla K80 GPU for few minutes:
![neural1](https://user-images.githubusercontent.com/35501699/47318652-3e020f80-d66a-11e8-9c78-53aded67dc8b.jpg)
![neural2](https://user-images.githubusercontent.com/35501699/47318653-3e9aa600-d66a-11e8-80df-5ad4db36a75c.jpg)
![neural3](https://user-images.githubusercontent.com/35501699/47318655-3e9aa600-d66a-11e8-92dc-9af6e87fb418.jpg)
![neural4](https://user-images.githubusercontent.com/35501699/47318657-3f333c80-d66a-11e8-92bc-0432eb4bed2e.jpg)







